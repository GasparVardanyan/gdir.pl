```
                         _ _            _
                __ _  __| (_)_ __ _ __ | |
               / _` |/ _` | | '__| '_ \| |
              | (_| | (_| | | | _| |_) | |
               \__, |\__,_|_|_|(_) .__/|_|
               |___/             |_|

gdir.pl -e key dir_to_encrypt out_dir     # encrypt
gdir.pl -d key encrypted_dir out_dir      # decrypt
gdir.pl -c key encrypted_dir filepath     # cat file
gdir.pl -l key encrypted_dir              # list files
gdir.pl -R key encrypted_dir              # rename files
```
