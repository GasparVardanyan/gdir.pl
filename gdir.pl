#!/usr/bin/env perl

use strict;
use warnings;

use File::Basename;
use File::Copy;
use File::Find;
use File::Path;
use File::Spec;

my $TIME_BASED_CHECK = $ENV{"GDIR_TIME_BASED_CHECK"};

{
	package gutils;

	sub file_sum {
		my $file_sum = `sha256sum "$_[0]"`;
		chomp ($file_sum);
		$file_sum =~ s/\s.*$//;

		return $file_sum;
	}

	sub confirm {
		my $ret = 0;

		while (<STDIN>) {
			chomp;

			/^(y|Y|yes|YES|Yes)$/ && do {
				$ret = 1;
				last;
			};

			/^(n|N|no|NO|No)$/ && do {
				$ret = 0;
				last;
			};
		}

		return $ret;
	}
}

sub encrypt {
	my $inp_key = $_[0];
	my $inp_idir = File::Spec->rel2abs ($_[1]);
	my $inp_odir = File::Spec->rel2abs ($_[2]);
	# my $inp_gargs = $_[3];

	mkdir $inp_odir;

	my $gdir="$inp_odir/.gcrypted";
	my $gdir_index="$gdir/gcrypted.index";
	my $gdir_file="$gdir/gcrypted.file";
	my $gdir_checksums="$gdir/gcrypted.checksums";

	my $reuse = 0;

	if (-f $gdir_index && -f $gdir_checksums) {
		print "have you used the same key last time? ";

		$reuse = gutils::confirm ();

		if (1 == $reuse) {
			if ($TIME_BASED_CHECK && "1337" eq $TIME_BASED_CHECK) {
				print
					"Time based check is enabled\n"
					. "This may result in data corruption\n"
					. "Think twice...\n"
					. "PROCEED? "
				;

				if (1 == gutils::confirm ()) {
					print "REALLY? ";

					if (0 == gutils::confirm ()) {
						return;
					}
				}
			}
		}
	}

	my (@reuse_files, @reuse_gfiles, @reuse_sums);

	if (1 == $reuse) {
		my @checksums = split /\n/
			, `cat "$gdir_checksums" | gcrypt -S -d "$inp_key"`
		;

		my $file_index = 0;

		open my $pipe
			, '-|'
			, "cat \"$gdir_index\" | gcrypt -S -d \"$inp_key\""
		;

		my $last_enc_date = (stat ($gdir_index)) [9];

		while (<$pipe>) {
			chomp;

			my $file_relpath = $_;
			my $file = "$inp_idir/$file_relpath";

			my $reuse_current_file = 0;

			if (-f $file) {
				my $enc_sum = $checksums[$file_index];

				if (
					$TIME_BASED_CHECK
					&& "1337" eq $TIME_BASED_CHECK
					&& $last_enc_date > (stat ($file)) [9]
				) {
					print "[$file_index] no write since last encryption\n";
					$reuse_current_file = 1;
				}
				else {
					print "[$file_index] "
						. "checking the sum of "
						. "$file_relpath... "
					;

					my $file_sum = gutils::file_sum ($file);

					if ($enc_sum eq $file_sum) {
						print "same\n";
						$reuse_current_file = 1;
					}
					else {
						print "\n\t...not same (will to be reencrypted)\n";
					}
				}

				if (1 == $reuse_current_file) {
					push @reuse_files, $file_relpath;
					push @reuse_gfiles, $file_index;
					push @reuse_sums, $enc_sum;
				}
				else {
				}
			}

			$file_index++;
		}

		close $pipe;

		print "proceed? ";

		if (0 == gutils::confirm ()) {
			return;
		}

		rename $gdir, "$gdir.old";
	}

	my $file_index = 0;

	rmdir $gdir;
	mkdir $gdir;

	my (@file_paths, @file_sums);

	find (
		{
			wanted => sub {
				my $file = $File::Find::name;
				if (-f $file) {
					my $file_relpath = $file;
						$file_relpath =~ s/^\Q$inp_idir\E\///;

					my $index = -1;

					for (my $i = 0; $i < @reuse_files; $i++) {
						if ($reuse_files[$i] eq $file_relpath) {
							$index = $i;
							last;
						}
					}

					my $newgfile = "$gdir_file$file_index";

					my $reused_old_gfile = 0;

					if (-1 != $index) {
						my $oldgfile = "$gdir.old/gcrypted.file"
							. "$reuse_gfiles[$index]"
						;
						if (-f $oldgfile) {
							print "[$file_index] "
								. "reusing the existing encryped file for "
								. "$file_relpath...\n"
							;

							move $oldgfile, $newgfile;
							push @file_sums, $reuse_sums[$index];
							$reused_old_gfile = 1;
						}
					}

					if (0 == $reused_old_gfile) {
						print "[$file_index] encrypting $file_relpath...\n";

						system "gcrypt -e \"$inp_key\" "
							. "\"$file\" \"$newgfile\"\n"
						;

						my $checksum = gutils::file_sum ($file);
						push @file_sums, $checksum;
					}

					push @file_paths, $file_relpath;

					$file_index++;
				}
			}
			# , follow => 1
		}
		, $inp_idir
	);

	rmtree "$gdir.old";

	sub gpipe {
		my ($key, $out, @arr) = @_;

		open my $pipe, '|-', "gcrypt -S -e \"$key\" > \"$out\"";
		map { print $pipe "$_\n"; } @arr;
		close $pipe;
	}

	gpipe $inp_key, $gdir_index, @file_paths;
	gpipe $inp_key, $gdir_checksums, @file_sums;
}

sub listgdir {
	my $inp_key = $_[0];
	my $inp_idir = File::Spec->rel2abs ($_[1]);
	my $gdir="$inp_idir/.gcrypted";

	my $gdir_index="$gdir/gcrypted.index";

	system "cat \"$gdir_index\" | gcrypt -S -d \"$inp_key\"";
}

sub printsums {
	my $inp_key = $_[0];
	my $inp_idir = File::Spec->rel2abs ($_[1]);
	my $gdir="$inp_idir/.gcrypted";

	my $gdir_index="$gdir/gcrypted.index";
	my $gdir_checksums="$gdir/gcrypted.checksums";

	my @files = split /\n/
		, `cat \"$gdir_index\" | gcrypt -S -d \"$inp_key\"`
	;

	my @checksums = split /\n/
		, `cat \"$gdir_checksums\" | gcrypt -S -d \"$inp_key\"`
	;

	for (my $i = 0; $i < @files; $i++) {
		print "$checksums[$i] $files[$i]\n";
	}
}

sub decrypt {
	my $inp_key = $_[0];
	my $inp_idir = File::Spec->rel2abs ($_[1]);
	my $inp_odir = File::Spec->rel2abs ($_[2]);
	# my $inp_gargs = $_[3];

	my $gdir="$inp_idir/.gcrypted";
	my $gdir_index="$gdir/gcrypted.index";
	my $gdir_file="$gdir/gcrypted.file";

	my @files = split /\n/
		, `cat \"$gdir_index\" | gcrypt -S -d \"$inp_key\"`
	;

	for (my $i = 0; $i < @files; $i++) {
		print "[$i] decrypting $files[$i]...\n";

		File::Path::make_path (File::Basename::dirname ("$inp_odir/$files[$i]"));

		system "gcrypt -d \"$inp_key\" "
			. "\"$gdir_file$i\" \"$inp_odir/$files[$i]\"\n"
		;
	}
}

sub catfile {
	my $inp_key = $_[0];
	my $inp_idir = File::Spec->rel2abs ($_[1]);
	my $inp_file = $_[2];

	my $gdir="$inp_idir/.gcrypted";
	my $gdir_index="$gdir/gcrypted.index";
	my $gdir_file="$gdir/gcrypted.file";

	my @files = split /\n/
		, `cat \"$gdir_index\" | gcrypt -S -d \"$inp_key\"`
	;

	for (my $i = 0; $i < @files; $i++) {
		if ($files[$i] eq $inp_file) {
			system "cat \"$gdir_file$i\" | gcrypt -S -d \"$inp_key\"";
			last;
		}
	}
}

sub renfiles {
	print "NOT IMPLEMENTED YET\n";
}

sub banner {
	print
		 "                         _ _            _ \n"
		."                __ _  __| (_)_ __ _ __ | |\n"
		."               / _` |/ _` | | '__| '_ \\| |\n"
		."              | (_| | (_| | | | _| |_) | |\n"
		."               \\__, |\\__,_|_|_|(_) .__/|_|\n"
		."               |___/             |_|      \n"
		."\n"
	;
}

sub help {
	banner ();

	print
		 "gdir.pl -e key dir_to_encrypt out_dir     # encrypt\n"
		."gdir.pl -d key encrypted_dir out_dir      # decrypt\n"
		."gdir.pl -c key encrypted_dir filepath     # cat file\n"
		."gdir.pl -l key encrypted_dir              # list files\n"
		."gdir.pl -R key encrypted_dir              # rename files\n"
	;
}

if (1 <= @ARGV) {
	if ('-e' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			encrypt ($ARGV[1], $ARGV[2], $ARGV[3]);
		}
	}
	elsif ('-d' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			decrypt ($ARGV[1], $ARGV[2], $ARGV[3]);
		}
	}
	elsif ('-l' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			listgdir ($ARGV[1], $ARGV[2]);
		}
	}
	elsif ('-s' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			printsums ($ARGV[1], $ARGV[2]);
		}
	}
	elsif ('-c' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			catfile ($ARGV[1], $ARGV[2], $ARGV[3]);
		}
	}
	elsif ('-r' eq $ARGV[0]) {
		if ('' ne $ARGV[1]) {
			renfiles ($ARGV[1], $ARGV[2]);
		}
	}
	elsif ('-h' eq $ARGV[0]) {
		if (1 == scalar @ARGV) {
			help ();
		}
	}
}
elsif (0 == @ARGV) {
	banner ();
}
